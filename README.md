## Overview

This Python script demonstrates how to use the [Context Locals](https://werkzeug.palletsprojects.com/en/2.0.x/local/) data storage from [Werkzeug](https://werkzeug.palletsprojects.com/en/2.0.x/). 

This script is part of my blog post on [TestDriven.io](https://testdriven.io/):

* [Deep Dive into Flask's Application and Request Contexts](https://testdriven.io/blog/flask-contexts-advanced/)

![Deep Dive into Flask's Application and Request Contexts](static/img/flask_contexts_advanced.png?raw=true "Deep Dive into Flask's Application and Request Contexts")

## Description

In [Flask](https://flask.palletsprojects.com/en/2.0.x/), the Application Context Stack and Request Context Stack are utilized when a request is processed in Flask. These stacks are implemented as `LocalStack` objects in global memory.

This script demonstrates how Context Locals from Werkzeug work by creating a `LocalStack` object in global memory and then having three separate threads access it:

![Context Locals Script Overview](static/img/flask_thread_local_example.png?raw=true "Context Locals Script Overview")

First, a `LocalStack` object (`thread_data_stack`) is created for storing data from each of the threads to be created.

> `thread_data_stack` mimics the Application Context Stack or Request Context Stack in Flask.

The `long_running_function()` is run in each of the threads:

```python
def long_running_function(thread_index: int):
    """Simulates a long-running function by using time.sleep()."""

    thread_data_stack.push({'index': thread_index, 'thread_id': threading.get_native_id()})
    print(f"Starting thread #{thread_index}... {thread_data_stack}")

    time.sleep(random.randrange(1, 11))

    print(f'LocalStack contains: {thread_data_stack.top}')
    print(f'Finished thread #{thread_index}!')
    thread_data_stack.pop()
```

This function pushes data about the thread to the `thread_data_stack` object in global memory:

```python
thread_data_stack.push({'index': thread_index, 'thread_id': threading.get_native_id()})
```

> This operation mimics the Application or Request context being pushed to their respective stack.

After the `time.sleep()` function completes, the data from the `thread_data_stack` is accessed:

```python
print(f'LocalStack contains: {thread_data_stack.top}')
```

> This operation mimics using the app_context and request proxies, as these proxies access the data on the top of their respective stack.

At the end of the function, the data is popped from the `thread_data_stack`:

```python
thread_data_stack.pop()
```

> This operation mimics popping the Application or Request context from their respective stack.

When the script is run, it will start 3 threads:

```python
# Create and start 3 threads that each run long_running_function()
for index in range(3):
    thread = threading.Thread(target=long_running_function, args=(index,))
    threads.append(thread)
    thread.start()
```

And join each thread so the script waits until each thread completes executing:

```python
# Wait until each thread terminates before the script exits by
# 'join'ing each thread
for thread in threads:
    thread.join()
```

Let's run this script to see what happens:

```sh
$ python app.py

Starting thread #0... <werkzeug.local.LocalStack object at 0x100a61280>
Starting thread #1... <werkzeug.local.LocalStack object at 0x100a61280>
Starting thread #2... <werkzeug.local.LocalStack object at 0x100a61280>
LocalStack contains: {'index': 1, 'thread_id': 1084407}
Finished thread #1!
LocalStack contains: {'index': 2, 'thread_id': 1084408}
Finished thread #2!
LocalStack contains: {'index': 0, 'thread_id': 1084406}
Finished thread #0!
Done!
```

What's really interesting about each thread is that they all point to the same `LocalStack` object in memory:

```sh
Starting thread #0... <werkzeug.local.LocalStack object at 0x100a61280>
Starting thread #1... <werkzeug.local.LocalStack object at 0x100a61280>
Starting thread #2... <werkzeug.local.LocalStack object at 0x100a61280>
```

When each thread accesses thread_data_stack, the access is **unique** to that thread! That's the magic of `LocalStack` (and `Local`) -- they allow context-unique access:

```sh
LocalStack contains: {'index': 1, 'thread_id': 1084407}
LocalStack contains: {'index': 2, 'thread_id': 1084408}
LocalStack contains: {'index': 0, 'thread_id': 1084406}
```

The access to `thread_data_stack` is also thread-safe, unlike typical global memory access.

## Installation Instructions

### Installation

Pull down the source code from this GitLab repository:

```sh
$ git clone git@gitlab.com:patkennedy79/werkzeug_context_locals_example.git
```

Create a new virtual environment:

```sh
$ cd werkzeug_context_locals_example
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages specified in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

### Running the Script

Run the script:

```sh
(venv) $ python app.py
```

## Notes

This script utilizes the following Python modules:
* **Werkzeug**: set of utilities for creating a Python application that can talk to a WSGI server (key module for Flask)

This application is written using Python 3.10.1.
